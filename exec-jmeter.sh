#!/bin/bash

FILE=${1-test1.jmx}

SLAVE_IP=$(docker inspect -f '{{.Name}} {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq) | grep slave | awk -F' ' '{print $2}' | tr '\n' ',' | sed 's/.$//')
WDIR=`docker exec -it master /bin/pwd | tr -d '\r'`
BASENAME=$(basename $FILE)
BASENAME="${BASENAME%.*}"

mkdir -p tests/results

eval "docker exec -it master /bin/sh -c 'jmeter -n -t $FILE -l results/$BASENAME.jtl -R$SLAVE_IP'"

#for filename in tests/*.jmx; do
#    NAME=$(basename $filename)
#    NAME="${NAME%.*}"
#
#    echo $filename
#
#    #eval "docker cp $filename master:$WDIR/"
#    #eval "docker exec -it master /bin/sh -c 'mkdir $NAME && cd $NAME && jmeter -n -t ../$filename -R$SLAVE_IP'"
#    #eval "docker cp master:$WDIR/$NAME tests/results/"
#done

